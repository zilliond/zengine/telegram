<?php

use Modules\Telegram\Http\Controllers\OperationController;
use Modules\Telegram\Http\Controllers\TicketController;
use Modules\Telegram\Http\Controllers\UserController;
use Modules\Telegram\Http\Controllers\TelegramController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('telegram')->group(static function () {
    Route::post('login', [TelegramController::class, 'login']);

    Route::middleware('auth:api')->group(static function () {
        Route::get('operations', [OperationController::class, 'index']);
        Route::get('operations/{operation}/reject', [OperationController::class, 'reject']);
        Route::get('operations/{operation}/confirm', [OperationController::class, 'confirm']);

        Route::get('stats', [TelegramController::class, 'stats']);
        Route::get('balances', [TelegramController::class, 'payments_balances']);

        Route::get('users', [UserController::class, 'index']);
        Route::post('users', [UserController::class, 'store']);
        Route::get('users/search', [UserController::class, 'search']);
        Route::post('users/check', [UserController::class, 'check']);
        Route::post('users/{user}/delete', [UserController::class, 'delete']);

        Route::get('tickets', [TicketController::class, 'index']);
        Route::post('tickets', [TicketController::class, 'store']);
        Route::get('tickets/{ticket}/messages', [TicketController::class, 'messages']);
        Route::post('tickets/{ticket}/messages', [TicketController::class, 'sendMessage']);
        Route::post('tickets/{ticket}/messages/{ticketMessage}/delete', [TicketController::class, 'deleteMessage']);
        Route::post('tickets/{ticket}/delete', [TicketController::class, 'delete']);
        Route::post('tickets/{ticket}/mark', [TicketController::class, 'mark']);
    });
});
