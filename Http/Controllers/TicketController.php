<?php

namespace Modules\Telegram\Http\Controllers;

use Modules\Core\Models\Ticket;
use Modules\Core\Models\TicketMessage;
use Modules\Core\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Controller;

class TicketController extends Controller
{
    public function index(Request $request)
    {
        return app('zengine')->model('Ticket')->with('user', 'messages')
            ->latest()
            ->when(request('user_id'), function (Builder $when) {
                return $when->where('user_id', request('user_id'));
            })
            ->paginate(5);
    }

    public function delete(Ticket $ticket)
    {
        return [
            'status' => $ticket->delete() ? 'success' : 'error'
        ];
    }

    public function store(Request $request)
    {
        $ticket_data = $request->only(['user_id', 'subject', 'status']);
        $ticket = app('zengine')->model('Ticket')->create($ticket_data);
        $message_data = [
            'read'      => 0,
            'user_id'   => \Auth::id(),
            'message'   => $request->get('message')
        ];
        $ticket->messages()->create($message_data);
        return $ticket->loadMissing(['user', 'messages']);
    }

    public function mark(Ticket $ticket, Request $request)
    {
        $ticket->status = $request->get('status');
        $status = $ticket->save();
        return [
            'ticket' => $ticket->loadMissing(['user', 'messages']),
            'status' => $status ? 'success' : 'error'
        ];
    }

    public function messages(Ticket $ticket)
    {
        return $ticket->messages()->with('user')
            ->latest()
            ->paginate(5);
    }

    public function sendMessage(Ticket $ticket, Request $request)
    {
        $request->validate([
            'message' => 'string|min:5'
        ]);
        $message = $ticket->messages()->create([
            'user_id'   => \Auth::id(),
            'message'   => $request->get('message')
        ]);
        return [
            'message' => $message->loadMissing('user')
        ];
    }

    public function deleteMessage(Ticket $ticket, TicketMessage $ticketMessage)
    {
        return [
            'status' => $ticketMessage->delete() ? 'success' : 'error'
        ];
    }
}
